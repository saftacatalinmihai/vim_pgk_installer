execute pathogen#infect()
syntax on
filetype plugin indent on
set mouse=a
nmap <F7> :NERDTree<CR> 
nmap <F8> :TagbarToggle<CR>

set noruler
set laststatus=2
set backspace=2
set splitright
set splitbelow
set hlsearch

set background=dark
colorscheme mirodark

set t_Co=256
highlight SignColumn ctermbg=18

let g:airline_theme            = 'dark'
let g:airline_enable_branch    = 1
let g:airline_enable_syntastic = 1
let g:airline_powerline_fonts  = 1


set statusline=
set statusline+=%<\                       " cut at start
set statusline+=%{fugitive#statusline()}  " fugitive vim git status
set statusline+=%2*[%n%H%M%R%W]%*\        " flags and buf no
set statusline+=%-40f\                    " path
set statusline+=%=%1*%y%*%*\              " file type
set statusline+=%10((%l,%c)%)\            " line and column
set statusline+=%P                        " percentage of file

let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'

let g:ctrlp_working_path_mode = 'ra'

set wildignore+=*.so,*.swp,*.zip 
