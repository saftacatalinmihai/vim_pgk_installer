#!/bin/bash - 
#===============================================================================
#
#          FILE: vim_package.sh
# 
#         USAGE: ./vim_package.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Safta Catalin Mihai, 
#  ORGANIZATION: 
#       CREATED: 08/27/2014 08:01:15 PM EEST
#      REVISION:  ---
#===============================================================================

CRT_DIR=$(pwd)
#===  Download and install vim =================================================
# if [ ! -d vim ]; then
# 	hg clone https://vim.googlecode.com/hg/ vim
# else
# 	cd vim
# 	hg update
# 	cd ..
# fi
# cd vim/src
# make
#===============================================================================

## Install powerline-fonts
#mkdir -p ~/.fonts
#cd ~/.fonts
#git clone https://github.com/Lokaltog/powerline-fonts.git
#fc-cache -vf ~/.fonts

#=== Install Pathogen ==========================================================
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
#===============================================================================

# Install Perl Support
cd /tmp
wget -O perl-support.zip http://www.vim.org/scripts/download_script.php?src_id=21738
cd ~/.vim
unzip /tmp/perl-support.zip
cd ~/.vim/perl-support/templates
sed -i -e 's/use strict;/use 5.14.1;\nuse strict;/' -e 's/use warnings;/use warnings;\nuse Data::Printer;/' comments.templates

cd ~/.vim/bundle

# Install NerdTree
git clone https://github.com/scrooloose/nerdtree.git

# Install TagBar
git clone https://github.com/majutsushi/tagbar.git

# Install Tabular
git clone git://github.com/godlygeek/tabular.git

# Install Surround
git clone git://github.com/tpope/vim-surround.git

# Install Supertab
git clone https://github.com/ervandew/supertab.git

# Install Syntastic
git clone https://github.com/scrooloose/syntastic.git

# Install NerdCommenter
git clone https://github.com/scrooloose/nerdcommenter.git

# Install vim-airline
git clone https://github.com/bling/vim-airline ~/.vim/bundle/vim-airline

# Install Gitgutter
git clone https://github.com/airblade/vim-gitgutter.git

# Install vim-fugitive
git clone https://github.com/tpope/vim-fugitive.git
vim -u NONE -c "helptags vim-fugitive/doc" -c q

# Install CtrlP
git clone https://github.com/kien/ctrlp.vim.git

# Install Unite
git clone https://github.com/Shougo/unite.vim.git

# Copy the .vimrc file to $HOME
cd $CRT_DIR
cp .vimrc ~/
mkdir -p ~/.vim/vim-support/templates
cp Templates ~/.vim/vim-support/templates/Templates

